import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Header from "./components/Header/Header";
import Home from "./components/Pages/Home";
import PostCreation from "./components/Pages/PostCreation";
import PostDetails from "./components/Pages/PostDetails";
import Login from "./components/Pages/Login";
import { UserContextProvider } from "./components/context/UserContext";
import PostEdition from "./components/Pages/PostEdition";

import "./App.css";

function App() {
  return (
    <div className="App">
      <div className="App-container">
        <UserContextProvider>
          <Router>
            <Header />
            <Switch>
              <Route exact path="/" component={Home}></Route>
              <Route component={Login} path="/login"></Route>
              <Route path="/new" component={PostCreation}></Route>
              <Route component={PostDetails} path="/details"></Route>
              <Route component={PostEdition} path="/edit"></Route>
            </Switch>
          </Router>
        </UserContextProvider>
      </div>
    </div>
  );
}

export default App;

import { useCallback, useContext } from "react";

import Context from "../context/UserContext";
import loginService from "../services/login";

function useUser() {
  const { token, setToken } = useContext(Context);

  const login = useCallback(
    ({ email, password }) => {
      loginService({ email, password })
        .then((token) => {
          console.log(token);
          window.localStorage.setItem("token", token);
          setToken(token);
        })
        .catch((err) => {
          console.log(err);
          window.localStorage.removeItem("token");
        });
    },
    [setToken]
  );

  const logout = useCallback(() => {
    window.localStorage.removeItem("token");
    setToken(null);
  }, [setToken]);

  return {
    isLogged: Boolean(token),
    login,
    logout,
  };
}

export default useUser;

import api from "./apiPosts";

const getAll = () => {
  return api.get("/posts");
};

const get = (id) => {
  return api.get(`/posts/${id}`);
};

const create = (data) => {
  return api.post("/posts", data);
};

const update = (id, data) => {
  return api.put(`/posts/${id}`, data);
};

const remove = (id) => {
  return api.delete(`/posts/${id}`);
};

const apiActions = {
  getAll,
  get,
  create,
  update,
  remove,
};

export default apiActions;

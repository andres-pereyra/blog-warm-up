import React from "react";
import { Link } from "react-router-dom";

import useUser from "../hooks/useUser";

import "bootstrap/dist/css/bootstrap.min.css";

function Header(props) {
  const { isLogged, logout } = useUser();

  const handleClick = (e) => {
    e.preventDefault();
    logout();
  };

  return (
    <nav className="header navbar navbar-dark bg-dark">
      <Link to="/">Home</Link>
      {"  "}
      {/* <Link to="/new" posts={props.posts} setPosts={props.setPosts}>New Post</Link> */}
      {"  "}
      {isLogged ? (
        <Link href="#" onClick={handleClick} to="*">
          Logout
        </Link>
      ) : (
        <Link to="login">Login</Link>
      )}
    </nav>
  );
}

export default Header;
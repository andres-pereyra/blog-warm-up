import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import PostsResultsItem from "./PostsResultsItem";

import "../../App.css";

const PostsResults = (props) => {
  const { posts, setPosts } = props;

  return (
    <div>
      <table className="table table-dark">
        <thead>
          <tr>
            <th scope="col">Title</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {posts.map((post) => (
            <PostsResultsItem post={post} posts={posts} setPosts={setPosts} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default PostsResults;

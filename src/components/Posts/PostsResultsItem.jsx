import React from "react";
import { NavLink } from "react-router-dom";

import crudPosts from "../services/crudPosts";

const PostsResultsItem = (props) => {
  const { post, posts, setPosts } = props;

  const handleRemove = async (id) => {
    try {
      const res = await crudPosts.remove(post.id);
      console.log(res.data);
      const newList = posts.filter((post) => post.id !== id);
      setPosts(newList);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <tr className="table-dark" key={post && post.id}>
      <td>{post && post.title}</td>
      <td>
        <NavLink
          className="btn btn-primary"
          to={{
            pathname: "/details",
            state: post,
          }}
        >
          Details
        </NavLink>
        {"  "}
        <NavLink
          className="btn btn-warning"
          to={{
            pathname: "/edit",
            state: post,
          }}
        >
          Edit
        </NavLink>
        {"  "}
        <button
          onClick={() => handleRemove(post.id)}
          type="button"
          className="btn btn-danger"
        >
          Delete
        </button>
      </td>
    </tr>
  );
};

export default PostsResultsItem;

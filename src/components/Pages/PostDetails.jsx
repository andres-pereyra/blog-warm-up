import React, { useEffect } from "react";
import { Link } from "react-router-dom";

import crudPosts from "../services/crudPosts";

import "bootstrap/dist/css/bootstrap.min.css";
import "../../App.css";

function PostsDetails(props) {
  const post = props.location.state;

  useEffect(() => {
    const searchPost = async () => {
      try {
        const res = await crudPosts.get(post.id);
        console.log(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    searchPost();
  }, [post.id]);

  return (
    <div className="container">
      <div className="card text-white bg-dark mb-3">
        <div className="card-body">
          <h3 className="card-title">
            <strong>{post.title}</strong>
          </h3>
          <p className="card-text">{post.body}</p>
          <Link to="/">Go Back</Link>
        </div>
      </div>
    </div>
  );
}

export default PostsDetails;
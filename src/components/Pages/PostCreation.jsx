import React, { useEffect } from "react";
import { useFormik } from "formik";
import { useHistory } from "react-router-dom";

import useUser from "../hooks/useUser";
import crudPosts from "../services/crudPosts";

const CreatePost = (props) => {
  const { isLogged } = useUser();

  const history = useHistory();
  const handlePostAdd = async (values) => {
    try {
      const res = await crudPosts.create(values);
      console.log(res.data);
      /* props.setPosts((prevPosts) => {
        return [...prevPosts, res.data]}) */
      props.setPosts([...props.posts, res.data]);
      /* window.localStorage.setItem("newPost", JSON.stringify(props.posts));
      window.localStorage.getItem("newPost") */
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (!isLogged) return history.push("/login");
  }, [history, isLogged]);

  const formik = useFormik({
    initialValues: {
      title: "",
      body: "",
    },

    validate: (values) => {
      let errors = {};

      if (!values.title) {
        errors.title = "Required";
      }

      if (!values.body) {
        errors.body = "Required";
      }

      return errors;
    },

    onSubmit: (values, { resetForm }) => {
      handlePostAdd(values);
      resetForm({ values: "" });
      history.push("/");
    },
  });

  return (
    <div className="container border border-green mt-5 pt-4">
      <form onSubmit={formik.handleSubmit} className="Form">
        <div className="mb-3">
          <label htmlFor="title">Title</label>
          <input
            id="title"
            name="title"
            type="title"
            onChange={formik.handleChange}
            value={formik.values.title}
            className="form-control"
          />
          {formik.errors.title && (
            <div className="error">{formik.errors.title}</div>
          )}
        </div>

        <div className="mb-3">
          <label htmlFor="body">Body</label>
          <input
            id="body"
            name="body"
            type="body"
            onChange={formik.handleChange}
            value={formik.values.body}
            className="form-control"
          />
          {formik.errors.body && (
            <div className="error">{formik.errors.body}</div>
          )}
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
};

export default CreatePost;

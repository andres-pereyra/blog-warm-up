import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

import PostsResults from "../Posts/PostsResults";
import crudPosts from "../services/crudPosts";
import useUser from "../hooks/useUser";
import PostCreation from "./PostCreation";

import "../../App.css";

const Home = (props) => {
  const [posts, setPosts] = useState([]);

  const { isLogged } = useUser();

  const history = useHistory();

  useEffect(() => {
    if (!isLogged) return history.push("/login");
    const searchPosts = async () => {
      try {
        const res = await crudPosts.getAll();
        setPosts(res.data);
        console.log(res.data);
      } catch (error) {
        console.log(error);
      }
    };
    searchPosts();
  }, [history, isLogged, setPosts]);

  return (
    <div className="container">
      <PostCreation posts={posts} setPosts={setPosts} />
      <PostsResults posts={posts} setPosts={setPosts} />
    </div>
  );
};

export default Home;
